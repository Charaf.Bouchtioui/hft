import pandas as pd 
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV

def load_data(paths):
    assert len(paths)==3, 'Paths doit contenir trois éléments'
    X = pd.read_csv(paths[0])
    X_test = pd.read_csv(paths[1])
    y_trader = pd.read_csv(paths[2])
    assert "Trader" in y_trader.columns.values, "le fichier y doit contenir une colonne Trader"
    assert "type" in y_trader.columns.values, "le fichier y doit contenir une colonne type"
    X = X.merge(y_trader,on='Trader')
    X["type"] = X["type"].replace({"HFT":1,"NON HFT":0,"MIX":2})
    return X, X_test, y_trader

def prepare_split_data(X,stratify,test_size=0.2,drop_columns=["Index","Share","Day","Trader"]):
    y = X["type"]
    X = X.drop(columns=['type'])
    if stratify:
        X_train, X_validation, y_train, y_validation = train_test_split(X, y,stratify=y, test_size=test_size)
    else:
        X_train, X_validation, y_train, y_validation = train_test_split(X, y,test_size=test_size)
    
    traders_train = X_train["Trader"]
    traders_validation = X_validation["Trader"]
    X_train = X_train.drop(columns=drop_columns)
    X_validation = X_validation.drop(columns=drop_columns)
    return X_train, X_validation, y_train, y_validation, traders_train, traders_validation

def find_missing(X):
    missing_values_cols = X.isnull().sum(axis = 0)
    missing_values_cols = missing_values_cols[missing_values_cols>0]
    missing_values_cols = list(missing_values_cols.index)
    return missing_values_cols

def get_percentage(df,y,traders,model):
    df = df.copy()
    df["row_class"] = model.predict(df)
    df["Trader"] = traders
    results = df.groupby(["Trader","row_class"])['OCR'].count()
    results = results.unstack(level=-1)
    results.fillna(0,inplace=True)
    results.rename(columns={0: "NON_HFT",1:"HFT",2:"MIX"},inplace=True)
    results["Total"] = results["HFT"] + results["NON_HFT"] + results["MIX"]
    results["HFT_percentage"] = results["HFT"] / results["Total"]
    results["MIX_percentage"] = results["MIX"] / results["Total"]
    results = results.merge(y,on="Trader")
    results["type"] = results["type"].replace({"NON HFT":0,"HFT":1,"MIX":2})
    results.dropna(inplace=True)
    return results


def hypertunning_models(models,grids,names,X,y,scoring,cv=5):
    for i in range(len(models)):
        search = RandomizedSearchCV(models[i],grids[i],scoring=scoring,cv=cv,n_jobs=-1)
        search.fit(X,y)
        print("the best params for the model {} are {} and with a score {}".format(names[i],search.best_params_,search.best_score_))









