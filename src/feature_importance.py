import pandas as pd
import numpy as np
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif, mutual_info_classif
from imputer import MaxImputer
from utils import find_missing
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.feature_selection import f_classif, mutual_info_classif




def rank_features(X_train, y_train, method):
    cols = X_train.columns.values
    impute_missing= MaxImputer(find_missing(X_train))
    X_train = impute_missing.fit_transform(X_train)
    if method == "anova":
	    fs = SelectKBest(score_func=f_classif, k='all')
    else:
        fs = SelectKBest(score_func=mutual_info_classif, k='all')
    fs.fit(X_train, y_train)
    results = {cols[i]:fs.scores_[i] for i in range(len(fs.scores_))}
    return results

def display_ranking(fs):
    df = pd.DataFrame.from_dict(fs, orient='index',columns=['Score'])
    df = df.sort_values(by='Score',ascending=False)
    plt.bar([i for i in range(df.shape[0])], df["Score"])
    plt.show()
    return df


def rank_features_RF(X_train,y_train):
    cols = X_train.columns.values
    impute_missing= MaxImputer(find_missing(X_train))
    X_train = impute_missing.fit_transform(X_train)
    clf = RandomForestClassifier().fit(X_train,y_train)
    results = {cols[i]:clf.feature_importances_[i] for i in range(len(cols))}
    df = display_ranking(results)
    return df


def find_best_k(X_train,y_train,model,method,scoring):
    cv = StratifiedKFold(n_splits=5)
    model = model
    impute_missing= MaxImputer(find_missing(X_train))
    if method == "anova" :
        selector = SelectKBest(score_func=f_classif)
    else:
        selector = SelectKBest(score_func=mutual_info_classif)
    pipeline = Pipeline(steps=[('imputer',impute_missing),('selector',selector), ('lr', model)])
    grid = dict()
    grid['selector__k'] = [i+1 for i in range(X_train.shape[1])]
    search = GridSearchCV(pipeline, grid, scoring=scoring, n_jobs=-1, cv=cv)
    results = search.fit(X_train, y_train)
    return results.best_score_,results.best_params_

def compare_score_k(features,X_train,y_train,model,scoring):
    cv = StratifiedKFold(n_splits=5)
    model = model
    scores = []
    for k in range(1,len(features)):
        if len(find_missing(X_train[features[:k]])) > 0:
            impute_missing= MaxImputer(find_missing(X_train[features[:k]]))
            pipeline = Pipeline(steps=[('imputer',impute_missing),('classifier', model)])
        else:
            pipeline = model
        score = cross_val_score(pipeline,X_train[features[:k]],y_train,cv=cv,n_jobs=-1,scoring=scoring).mean()
        scores.append(score)
    df = pd.DataFrame({"Number_of_features":[i for i in range(1,len(features))],
                            "Score":scores})
    df = df.sort_values(by='Score',ascending=False)
    return df







