import pandas as pd
import numpy as np
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
import pickle

class BestClassifier(object):
    def __init__(self,models,scoring,names,cv=5):
        self.models = models
        self.scoring = scoring
        self.names = names
        self.best_model = None
        self.best_score = 0
        self.cv = cv
    
    def search(self,X,y):
        name_index = 0
        for index,model in enumerate(self.models):
            score = cross_val_score(model, X, y,cv=self.cv,scoring=self.scoring,n_jobs=-1).mean()
            if score > self.best_score:
                self.best_score = score
                self.best_model = model
                name_index = index
                
        print("The best model is {} with a cross validation score {}:".format(self.names[name_index],self.best_score))

    def fit_save(self,X,y,path):
        if not self.best_model is None:
            classifier = self.best_model.fit(X,y)
            pickle.dump(classifier, open(path, 'wb'))


def predict_class_rows(X,model,fs=None,drop_columns = ["Index","Share","Day","Trader"]):
    traders = X["Trader"]
    X = X.drop(columns=drop_columns)
    if fs:
        X = X[fs]
    X["row_class"] = model.predict(X)
    X["Trader"] = traders
    return X

def predict_class_trader(df,model):
    df = df.copy()
    results = df.groupby(["Trader","row_class"])['OCR'].count()
    results = results.unstack(level=-1)
    results.fillna(0,inplace=True)
    results.rename(columns={0: "NON_HFT",1:"HFT",2:"MIX"},inplace=True)
    results["Total"] = results["HFT"] + results["NON_HFT"] + results["MIX"]
    results["HFT_percentage"] = results["HFT"] / results["Total"]
    results["MIX_percentage"] = results["MIX"] / results["Total"]
    results["type"] = model.predict(results[["HFT_percentage","MIX_percentage"]])
    results["type"] = results["type"].replace({0: "NON HFT",1:"HFT",2:"MIX"})
    results = pd.DataFrame({"Trader":results.index.values,"type":results["type"]})
    results.reset_index(drop=True,inplace=True)
    return results

def predict(X,model_row,model_trader,fs=None,drop_columns=["Index","Share","Day","Trader"]):
    if fs:
        df = predict_class_rows(X,model_row,fs)
    else:
        df = predict_class_rows(X,model_row)
    return predict_class_trader(df,model_trader)

def plot_test(df,model):
    df = df.copy()
    results = df.groupby(["Trader","row_class"])['OTR'].count()
    results = results.unstack(level=-1)
    results.fillna(0,inplace=True)
    results.rename(columns={0: "NON_HFT",1:"HFT",2:"MIX"},inplace=True)
    results["Total"] = results["HFT"] + results["NON_HFT"] + results["MIX"]
    results["HFT_percentage"] = results["HFT"] / results["Total"]
    results["MIX_percentage"] = results["MIX"] / results["Total"]
    results["type"] = model.predict(results[["HFT_percentage","MIX_percentage"]])
    plt.scatter(results["HFT_percentage"],results["MIX_percentage"], c=results["type"],cmap='viridis')
    plt.show()






