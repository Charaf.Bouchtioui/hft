import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin



class MaxImputer(BaseEstimator, TransformerMixin):
    def __init__(self, cols):
        assert type(cols) == list, 'cols should be a list of columns'
        
        self.cols = cols
        self.max_values = {}
        
    def fit(self, X, y=None):
        
        self.max_values = {col:X[col].max() for col in self.cols}
        return self 
    
    def transform(self, X, y=None):
        
        X = X.copy()
        
        X.fillna(value=self.max_values,inplace=True)
        
        return X.values

