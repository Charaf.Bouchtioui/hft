#  ENS Data challenge - Who are the high-frequency traders ? 

## Video URL 

[https://drive.google.com/file/d/1HZn_nNDbjWpvQvARsxwGPTRsj6FITsVr/view?usp=sharing](https://drive.google.com/file/d/1HZn_nNDbjWpvQvARsxwGPTRsj6FITsVr/view?usp=sharing).


## Project Description

The goal of the project is to classify market players into three categories HFT (high frequency trader), NON HFT et MIX (mix between both categories). 
We aim to obtain a classification score at least better than the benchmarked score (f1_score=0.90) with less features. 
The total number of features is 35. With our model we managed to obtain a classification score of 0.9285 using only 14 features.

The reader is referred to [the home page of the challenge](https://challengedata.ens.fr/participants/challenges/50/).


## Files Description 

.
 * [data](./data)
   * [AMF_train_X_XCZw8r3.csv](./data/AMF_train_X_XCZw8r3.csv) : Traning data (only features)
   * [AMF_train_Y_omYQJZL.csv](./data/AMF_train_Y_omYQJZL.csv) : Traders with their corresponding labels
   * [AMF_test_X_uDs0jHH.csv](./data/AMF_test_X_uDs0jHH.csv) : Test data
 * [models](./models) : Contains trained models saved with using the pickle python module
 * [submissions](./submissions) : solution files in csv format
 * [src](./src)
   * [utils](./src/utils.py) : utils.py contains multiple functions for loading data, hypertunning classifiers, transforming dataframes for usage in the notebooks
   * [classifier](./src/classifier.py) : Provides classes and functions for choosing the best classifiers and making predictions for both the trader's class and the class of rows (a row presents the statistic summary of the trader activity for a certain day and a certain stock)
   * [imputer](./src/imputer.py) : Provides a class of a customized sklearn transformer to imput missing values
   * [feature_importance.py](./src/feature_importance.py) : Methods to select the best subset of features
 * [notebooks](./notebooks)
   * [draf_first_model](./data/notebooks/draft_first_model.ipynb) : The first model used (classifying rows into HFT or NON HFT, then finding the optimal threshold of the HFT rows percentage to classify traders into HFT, NON HFT, MIX)
   * [final_model](./data/notebooks/final_model.ipynb) : The final model taking into account features selection
   * [feature_analysis](./data/notebooks/feature_analysis.ipynb) : Feature analysis and selection

## Dependencies 

* [pandas](https://pandas.pydata.org/)
* [sklearn](https://scikit-learn.org/stable/)
* [numpy](https://numpy.org/)
* [Matplotlib](https://matplotlib.org/)

## Usage 

### Install Dependencies 

> pip install numpy

> pip install matplotlib

> pip install pandas

> pip install -U scikit-learn

### Running notebooks

Each notebook can be run independantly. The principal notebooks are feature_analysis.ipynb and final_model.ipynb, they present the analysis done to select the optimal model with the convenient set of parameters. 
To run a notebook, open a terminal in the root directory of the project and run the command :

> jupyter notebook

In the web browser interface open the notebook and run the cells sequentially. 

## Final models and submissions

In the notebook final_model.ipynb, the row model and traders model get updated and downloaded to the ./models directory. 
The most recent models in this directory are : 
* best_row_model.sav : Best model so far for classifying rows without feature selection
* best_row_model_fs_anova.sav : Best model so far for classifying rows with feature selection using anova
* best_row_model_fs_fs_RF.sav : Best model so far for classifying rows with feature selection using Random forests feature importance

The most recent submission file is [./submissions/final_submission.csv](./submissions/final_submission.csv)


  

   

